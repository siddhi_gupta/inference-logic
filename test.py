'''dict = { "H(x)" : "iuiob", "B(x,y)" : "jvuvd", "D(x,y)" : "iuiob", "Q(y)" : "hfyfiu" }
query = ["H(John)", "B(John,Alice)", "B(John,Bob)", "D(John,Alice)", "Q(Bob)", "D(John,Bob)"]
keys = dict.keys()
substitution = {}
flag = False
for k in range(0, len(query)):
	for i in range(0, len(keys)):
		if keys[i].startswith(query[k].split("(")[0]):
			conclusion = dict[keys[i]]
			val1 = query[k].split("(")[1].split(")")[0].split(",")
			val2 = keys[i].split("(")[1].split(")")[0].split(",")
			if len(val1) == len(val2):
				for j in range(0, len(val1)):
					if val2[j].islower and val1[j].isupper:
						key = val2[j]
						value = val1[j]
						if key in substitution.keys():
							substitution[key].append(value)
						else:
							substitution[key] = [value]		
print conclusion
print val1
print val2
print substitution'''
'''
list1 = [1, 2, 3, 4, 5]
list2 = [1, 2, 3, 4, 5]
print cmp(list1, list2)
'''

'''
UNIFY ONE

f = open('input.txt', 'r')
f1 = open('output.txt', 'w')

ask = []
facts = []
tell = {}
substitutions = {}
output = False
noQueries = f.readline().strip('\n');
noQueries = int(noQueries)
print "noQueries" + str(noQueries)
for i in range(0, noQueries):
	ask.append(f.readline().strip('\n'))
print "ask" + str(ask)	
	
noKB = f.readline().strip('\n');
print "noKB" + str(noKB)
noKB = int(noKB)
for i in range(0, noKB):
	line = f.readline().strip('\n')
	if line.find("=>") == -1:
			facts.append(line)
			key = line.split("(")[0]
			value = ((line.split("(")[1]).split(")")[0]).split(",")
	else:
		key = line.split(" => ")[1]
		value = line.split(" => ")[0]
		if key in tell.keys():
			tell[key].append([value])
		else:
			tell[key] = [[value]]
print "tell" + str(tell)	
print "facts" + str(facts)

def unify (clause1, clause2, substitutions):
	if cmp(clause1, clause2) == 0:
		return (True, clause1, clause2)
	functionClause1 = clause1.split("(")[0]
	args1 = clause1.split("(")[1].split(")")[0].split(",")
	functionClause2 = clause2.split("(")[0]
	args2 = clause2.split("(")[1].split(")")[0].split(",")
	print "args1" + str(args1)
	print "args2" + str(args2)
	changes = 0
	if len(args1) == len(args2):
		if args1[0][0].isupper() and args2[0][0].islower():
			print "True--"
			print len(args1)
			for arg in range(0, len(args1)):
				if args2[arg] in substitutions.keys() and args1[arg] == substitutions[args2[arg]][0]:
					args2[arg] = args1[arg]
					changes += 1
			print changes		
			if changes == len(args1):	
				print "I'm here true"		
				clause1 = functionClause1 + "(" + (",".join(args1)) + ")"
				clause2 = functionClause2 + "(" + (",".join(args2)) + ")"
				return (True, clause1, clause2)
	print "Damn I'm here"
	return (False, clause1, clause2)
 
def backwardChaining(query, tell, facts, substitutions):
	print "query" + str(query)
	conclusions = []
	canUnify = False
	if query in facts:
		print "I'm a fact"
		return True
	tellKeys = tell.keys()	
	for i in range(0, len(tellKeys)):
		if tellKeys[i].startswith(query.split("(")[0]):	
			conclusions = tell[tellKeys[i]]
	print "conclusions" + str(conclusions)
	while (True):
		if not conclusions:
			break
		conclusion = conclusions.pop()
		conclusion = conclusion.pop()
		conclusion = str(conclusion)
		print "conclusion" + conclusion
		canUnify, query, conclusion = unify(query, conclusion, substitutions)
		print "query after" + str(query)
		print "conclusion after" + str(conclusion)
		print "canUnify" + str(canUnify)
		if conclusion in facts:
			return True
		elif conclusion.find("^") == -1:
			print "I'm in elif"
			if backwardChaining(conclusion, tell, facts, substitutions):
				return True
			else:
				pass		
		else:
			conjugates = conclusion.split(" ^ ")	
			entailedConjugates = []
			print "Conjugates" + str(conjugates)
			print "Length of conjugates" + str(len(conjugates))
			for i in range(0, len(conjugates)):
				entailedConjugates.append(backwardChaining(conjugates[i], tell, facts, substitutions))
			print "Current Conjugate" + str(conjugates[i])
			print "entailedConjugates" + str(entailedConjugates)	
			if False in entailedConjugates:
				return False
			else:
				return True
	print "query" + str(query)		
	print "I didn't do anything"
	return False					





askCounter = 0
if ask[askCounter] in facts:
	output = True								
	print output
	f1.write(str(output))
else:
	tellKeys = tell.keys()	
	for i in range(0, len(tellKeys)):
		if tellKeys[i].startswith(ask[askCounter].split("(")[0]):
			constants = ask[askCounter].split("(")[1].split(")")[0].split(",")
			print "constants" + str(constants)
			variables = tellKeys[i].split("(")[1].split(")")[0].split(",")
			print "variables" + str(variables)
			if len(constants) == len(variables):
				for j in range(0, len(constants)):
					if variables[j][0].islower() and constants[j][0].isupper():
						key = variables[j]
						value = constants[j]
						substitutions[key] = [value]
	print "substitutions" + str(substitutions)					
	output = backwardChaining(ask[askCounter], tell, facts, substitutions)
	print output
	f1.write(str(output))	
'''