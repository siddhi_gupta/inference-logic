General Instructions
================
--------------------------------

1. Clone this repo directly. 
2. Edit the script using Sublime or any other text editor.
3. Have the input file in the required format.
4. Run the script using the following command `python inference.py –i inputFile`

Use of Readme
============
------------------------------

* Use Table of Contents to navigate through the sections
* Review the edit log for recent changes
	* Remember to add you signature and date after you review recent changes!
* When you make a change, create an entry at the top of the Edit Log with the date of the change and your signature.

Links
====
----------

* [Inference Logic](https://en.wikipedia.org/wiki/Inference)
* [Backward Chaining](https://en.wikipedia.org/wiki/Backward_chaining)
* [First-Order Logic](https://en.wikipedia.org/wiki/First-order_logic)

Table of Contents
==============
----------------------------------
[TOC]

Inference Logic - Backward Chaining
=====================================
----------------------------------------

## Introduction

> As mobiles, wearables and other tiny gadgets surround us more and more in our day to day life, we are sharing more and more information with enterprises that provide us with their services. But what do we lose in return? How much control do we have on over our private information? These questions seem simple to answer at the first glance. You share what you want to share. But, it’s not like that with the use of AI methods that can mine your shared data to infer your private non-shared personal information. In this assignment, we want to help users to find out what an enterprise can do with their data before giving permission to their applications to access that.

> We are going to implement a backward chaining system that gets the rules of data-mining and the abilities that an enterprise has at its disposal. Then it will help customers to find out if a certain type of personal information can be extracted by that enterprise if it gains access to another set of information about the user.

## Problem Statement

We are given a knowledge base and a number of queries. Our job is to determine if the queries can be inferred from the knowledge base or not. I have used backward chaining algorithm for solving this problem.

## Input

> The first line of the input will be the number of queries (n). Following n lines will be the queries, one per line. For each of them, we have to determine whether it can be proved from the knowledge base or not.
Next line of the input will contain the number of clauses in the knowledge base (m).

> Following, there will be m lines each containing a statement in the knowledge base. 
> **Each clause is in one of these two formats:**

> - p1 ∧ p2 ∧ ... ∧ pn => q
> - facts: which are atomic sentences. Such as p or ~p

> **All the p's and also q are either a literal such as HasPermission(Google,Contacts) or negative of a literal such as ~HasPermission(Google,Contacts).**

> - Queries will not contain any variables.
> - Variables are all single lowercase letters.
> - All predicates (such as HasPermission) and constants (such as Google) are case-sensitive. Alphabetical strings that begin with uppercase letters.
> - Each predicate has at least one argument. (There is no upper bound for the number of arguments). Same predicate will not appear with different number of arguments.
> - All of the arguments of the facts are constants. i.e. we can assume that there will be no fact such as HasPermission(x,Contacts) ( which says that everyone has permission to see the contacts!) in the knowledge base.
> - We can assume that the input format is exactly as it is described. There are no errors in the given input.

## Output

We have created a file named ‘output.txt’. For each query, determine if that query can be inferred from the knowledge base or not, one query per line. If true, we print “TRUE” and if not, we print “FALSE”.

Edit Log
=======
-----------------
## Complete Creation
> Date: 29/11/15
> 
> **Sections:**
> 
> * General Instructions
> * Use of Readme
> * Links
> * Problem Statement
> * Functions 
>
>
> **Read Signature**
> 
> * Siddhi Gupta: 29/11/15