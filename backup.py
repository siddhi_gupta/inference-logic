import copy
f = open('input.txt', 'r')
f1 = open('output.txt', 'w')

ask = []
facts = []
tell = {}
substitutions = {}
output = False
noQueries = f.readline().strip('\n').strip('\r');
noQueries = int(noQueries)
print "noQueries" + str(noQueries)
for i in range(0, noQueries):
	ask.append(f.readline().strip('\n').strip('\r'))
print "ask" + str(ask)	
	
noKB = f.readline().strip('\n').strip('\r');
print "noKB" + str(noKB)
noKB = int(noKB)
for i in range(0, noKB):
	line = f.readline().strip('\n').strip('\r')
	if line.find("=>") == -1:
			facts.append(line)
			key = line.split("(")[0]
			value = ((line.split("(")[1]).split(")")[0]).split(",")
	else:
		key = line.split(" => ")[1]
		value = line.split(" => ")[0]
		if key in tell.keys():
			tell[key].append([value])
		else:
			tell[key] = [[value]]
print "tell" + str(tell)	
print "facts" + str(facts)

def removeExtraValues(possibleValues, values):
	for key in possibleValues.iterkeys():
		if key not in values.keys():
			values[key] = copy.deepcopy(possibleValues[key])
		else:
			list1 = possibleValues[key]
			list2 = values[key]
			values[key] = set(list1).intersection(list2)	
			values[key] = list(values[key])
	return values	
	
def getSubstitution(tell, query):
	substitutions = {}
	for key in tell.keys():
		if key.startswith(query.split("(")[0]):
			constants = query.split("(")[1].split(")")[0].split(",")
			print "constants" + str(constants)
			variables = key.split("(")[1].split(")")[0].split(",")
			print "variables" + str(variables)
			if len(constants) == len(variables):
				for i in range(0, len(constants)):
					if variables[i][0].islower() and constants[i][0].isupper():
						key = variables[i]
						value = constants[i]
						substitutions[key] = [value]
	print "substitutions" + str(substitutions)	
	return 	substitutions			

def substitute(clause, substitutions):
	print clause
	function = clause.split("(")[0]
	new_arguments = []
	arguments = clause.split("(")[1].split(")")[0].split(",")
	print "arg"
	print arguments
	for argument in arguments:
		print "in"
		print substitutions.keys()
		print argument
		print argument in substitutions.keys()
		if argument in substitutions.keys():
			argument = copy.deepcopy(substitutions[argument])
			print argument
			
			argument = argument.pop()
			new_arguments.append(argument)
			print new_arguments
		else:
			new_arguments.append(argument)	
	clause = function + ("(" + ((",".join(new_arguments))) + ")")
	print clause
	return clause
	
def hasVariables(clause):
	clause = clause.split("(")[1].split(")")[0].split(",")
	for argument in clause:
		print argument[0]
		if argument[0].islower():
			return True
	return False			

def unify(actualQuery, subsQuery):
	list = {}
	print "Unify"
	aQuery = copy.deepcopy(actualQuery)
	sQuery = copy.deepcopy(subsQuery)
	aQuery = aQuery.split("(")[1].split(")")[0].split(",")
	sQuery = sQuery.split("(")[1].split(")")[0].split(",")
	print aQuery
	print sQuery
	eFlag = False
	if cmp(aQuery, sQuery) == 0:
		for i in range(0, len(aQuery)):
			if aQuery[i][0].islower():
				eFlag = True
		if eFlag == False:		
			return [0]
		else:
			eFlag = False	
	for i in range(0, len(aQuery)):
		if aQuery[i][0].islower() and sQuery[i][0].islower():
			key = aQuery[i]
			value = sQuery[i]
			if key != value:
				list[key] = value
	print list
	print "list"		
	return list	
	
def changeVariables(list, possibleValues):
	global repeatFlag
	for key in list.iterkeys():
		if list[key] in list.keys():
			repeatFlag = True
	print "I'm changing variables"
	print "list" + str(list)
	if not possibleValues:
		return possibleValues
	for key in list.iterkeys():
		print "key" + str(key)
		print list[key]
		if key == list[key]:
			continue
		print possibleValues
		print "huh"
		#print list[key] in possibleValues.keys()
		if list[key] in possibleValues.keys():
			print "ha"
			possibleValues[key] = possibleValues[list[key]]
			del possibleValues[list[key]]
      		print "new"
      	else:
      		print "Continuing"	
	print "What is the issue?"      		
	print possibleValues
	print "return smarty"
	return possibleValues
	
def equalConstants(conclusion, fact):
	print "I'm in equalConstants"
	print conclusion
	print fact
	factArgs = fact.split("(")[1].split(")")[0].split(",")	
	queryArgs = conclusion.split("(")[1].split(")")[0].split(",")
	for count in range(0, len(factArgs)):
		print queryArgs[count]
		print factArgs[count]
		if queryArgs[count][0].isupper():
			if queryArgs[count] != factArgs[count]:
				return False
	return True				
				
		
def backwardChaining(query, tell, facts, substitutions):
	global queries
	global conjugateList
	global repeatFlag
	print "query" + str(query)
	if query in queries:
		return (False, [])
	queries.append(query)
	variableList = []
	changedQuery = ""
	conclusions = []
	if query in facts:
		print "I'm a fact"
		return (True, [])
	implications = []	
	print "I'm still here"
	for key in tell.iterkeys():
		print "Looking"
		print key
		if key.startswith(query.split("(")[0]):	
			implications = copy.deepcopy(tell[key])
			print "implications" + str(implications)
			variableList = unify(query, key)
			if variableList == [0]:
				break
			if variableList:
				changedQuery = query
				key = changedQuery
				value = variableList
				conjugateList[key] = value
			print "VariableList" + str(variableList)
			a = 1
			for list in implications:
				if list == []:
					pass
				else:
					a = 0
				if a == 1:	
					print "That's why"	
					return (False, [])
	print "hasVariable"
	print hasVariables(query)
	print not implications
	if not implications and hasVariables(query) == False:
		print "I went here"
		return (False, [])
	elif not implications and hasVariables(query) == True:
		print "I've variables, check with facts"
		possibleValues = {}
		factFlag = False
		counterFlag = False
		for fact in facts:
			counter = 0 
			if fact.startswith(query.split("(")[0]):
				if equalConstants(query, fact) == False:
					continue
				factFlag = True
				factArgs = fact.split("(")[1].split(")")[0].split(",")	
				queryArgs = query.split("(")[1].split(")")[0].split(",")
				print factArgs
				print queryArgs
				variables = []
				for i in range(0, len(factArgs)):
					if queryArgs[i][0].islower():
						counter = counter + 1
						key = queryArgs[i]
						value = factArgs[i]
						if key in possibleValues.keys():
							print "Yo"
							print "variables" + str(variables)
							if key not in variables:
								possibleValues[key].append(value)
							else:
								possible = copy.deepcopy(possibleValues[key])
								alreadyValue = possible.pop()
								print alreadyValue
								print value
								if alreadyValue == value:
									continue
								else:	
									print "Reason"
									return(False, [])
						else:
							variables.append(key)
							print "variables after appending" + str(variables)
							possibleValues[key] = [value]
					else:
						print queryArgs[i]
						print factArgs[i]
						print "tada"
						if factArgs[i] != queryArgs[i]:
							print "Damn its false"
							factFlag = False
						else:
							counter = counter + 1
				if counter == len(factArgs):
					counterFlag = True					
		if factFlag == True or counterFlag == True:
			print "returning"
			print possibleValues
			return (True, possibleValues)
		else:
			print "Return false sad"
			return (False, [])					
	conclusions = []
	for conclusion in implications:
		conclusion = conclusion.pop()
		print conclusion
		if conclusion.find("^") == -1:
			conclusions.append(substitute(conclusion, substitutions))
		else:
			new_conjugates = []
			conjugates = conclusion.split(" ^ ")
			print conjugates
			for conjugate in conjugates:
				print conjugate
				conjugate = substitute(conjugate, substitutions)
				new_conjugates.append(conjugate)
			conclusions.append(" ^ ".join(new_conjugates))
	print "conclusions after substitute" + str(conclusions)
	while(conclusions):
		print "came back"
		conclusion = conclusions.pop()
		print conclusions
		print conclusion
		if conclusion in facts:
			print "Entered 1"
			return (True, [])
		elif conclusion.find("^") == -1 and hasVariables(conclusion) == False:
			print "Entered 2"
			output, possibleValues  = backwardChaining(conclusion, tell, facts, getSubstitution(tell, conclusion))
			print "case 1"
			print "Output " + str(output)
			print possibleValues
			if output == True:
					return (output, [])
			print "pass"
			print conclusions
			pass
		elif conclusion.find("^") == -1 and hasVariables(conclusion) == True:
			print "Entered 3"
			possibleValues = {}
			factFlag = False
			for fact in facts:
				print fact
				print conclusion.split("(")[0]
				if fact.startswith(conclusion.split("(")[0]):
					if equalConstants(conclusion, fact) == True:
						print "Yep"
						factFlag = True
						factArgs = fact.split("(")[1].split(")")[0].split(",")	
						queryArgs = conclusion.split("(")[1].split(")")[0].split(",")
						for i in range(0, len(factArgs)):
							if queryArgs[i][0].islower():
								key = queryArgs[i]
								value = factArgs[i]
								if key in possibleValues.keys():
									possibleValues[key].append(value)
								else:
									possibleValues[key] = [value]
			print possibleValues					
			for key in substitutions.keys():
				if key in possibleValues.keys():
					map(possibleValues.pop, [key])	
			if factFlag == True:
				print "Yellow"
				print possibleValues
				return (True, possibleValues)
			else:
				output, possibleValues  = backwardChaining(conclusion, tell, facts, getSubstitution(tell, conclusion))			
				print "Output " + str(output)
				print possibleValues
				print "changedQuery" + str(changedQuery)
				if query == changedQuery:
					possibleValues = changeVariables(variableList, possibleValues)
				print "I changed the variables, hurrah!"
				repeatFlag = False
				print possibleValues
				if output == True:
					return (output, possibleValues)
		elif conclusion.find("^") != -1:
			print "Entered 4"
			values = {}
			conjugates = conclusion.split(" ^ ")
			for conjugate in conjugates:
				output, possibleValues  = backwardChaining(conjugate, tell, facts, getSubstitution(tell, conjugate))
				print possibleValues
				print "I'm blue!"
				print output
				if output == False:
					return (False, [])
				else:	
					print "Case 2"
					print query
					print changedQuery
					print conjugate
					print conjugateList
					if conjugate in conjugateList.keys():
						variableList = conjugateList[conjugate]
						print "N"
						print variableList
						possibleValues = changeVariables(variableList, possibleValues)			
						print "Hey"
						#put the condition for xx
						if possibleValues:
							for key in possibleValues.iterkeys():
								if len(possibleValues[key]) == 1:
									sub = {}
									sub[key] = copy.deepcopy(possibleValues[key])
									for index, c in enumerate(conjugates):
										c = substitute(c, sub)
										conjugates[index] = c
							
					if possibleValues:
						if not values:
							print "values here happened"
							print conjugate
							values = copy.deepcopy(possibleValues)
						else:
							print "values" + str(values)
							values = removeExtraValues(possibleValues, values)
							print "values" + str(values)
							for key in values.keys():
								if not values[key]:
									return (False, [])
			if query == changedQuery:
				possibleValues = changeVariables(variableList, possibleValues)
				repeatFlag = False
			print "changedQuery" + str(changedQuery)						
			print values					
			conjugate = substitute(conjugate, values)	
			print "conjugate"
			print conjugate
								
			return (True, values)
	return (False, [])									

#askCounter = noQueries
for askCounter in range(0, noQueries):
	tell1 = copy.deepcopy(tell)
	conjugateList = {}
	substitutions = {}
	queries =[]
	repeatFlag = False
	print "====================================="
	print tell
	if ask[askCounter] in facts:
		output = TRUE								
		print output
		f1.write(str(output))
		f1.write("\n")
	else:
		substitutions = getSubstitution(tell, ask[askCounter])
		#try:
		output, possibleValues  = backwardChaining(ask[askCounter], tell1, facts, substitutions)
		'''except:
			output, possibleValues = (False, [])'''
		print "Final"
		print output
		print possibleValues
		output = str(output).upper()
		f1.write(str(output))
		f1.write("\n")